package com.carolynvs.stash.plugin.force_field;

import com.atlassian.stash.commit.CommitService;
import com.atlassian.stash.content.ChangesetsBetweenRequest;
import com.atlassian.stash.hook.HookResponse;
import com.atlassian.stash.hook.repository.PreReceiveRepositoryHook;
import com.atlassian.stash.hook.repository.RepositoryHookContext;
import com.atlassian.stash.i18n.I18nService;
import com.atlassian.stash.repository.RefChange;
import com.atlassian.stash.repository.RefChangeType;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.util.PageUtils;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PreventForcePushHook implements PreReceiveRepositoryHook
{
    private final CommitService commitService;
    private final I18nService i18nService;
    private final PathMatcher pathMater = new AntPathMatcher();

    public PreventForcePushHook(CommitService commitService, I18nService i18nService)
    {
        this.commitService = commitService;
        this.i18nService = i18nService;
    }

    @Override
    public boolean onReceive(@Nonnull RepositoryHookContext context, @Nonnull Collection<RefChange> refChanges, @Nonnull HookResponse response)
    {
        List<String> restrictedRefs = loadConfiguration(context);

        Repository repository = context.getRepository();

        List<String> blocked = new ArrayList<String>();
        for(RefChange refChange : refChanges)
        {
            String refId = refChange.getRefId();
            if(isRestrictedRef(restrictedRefs, refChange) && isForcePush(repository, refChange))
            {
                blocked.add(refId);
            }
        }

        if (!blocked.isEmpty())
        {
            response.out().println("=================================");
            response.out().println(i18nService.getText("force-field.plugin-name", "Force Field"));
            for (String refId : blocked)
            {
                response.out().println(i18nService.getText("force-field.error-message", "The repository administrator has disabled force pushes to {0}", refId));
            }
            response.out().println("=================================");
        }

        return blocked.isEmpty();
    }

    private List<String> loadConfiguration(@Nonnull RepositoryHookContext context)
    {
        ArrayList<String> restrictedRefs = new ArrayList<String>();

        String[] refs = context.getSettings().getString("references").split(" ");
        for(String ref : refs)
        {
            // any pattern that falls outside refs/ would match nothing, so is considered a suffix match
            if (!ref.startsWith("**") && !ref.startsWith("refs/"))
            {
                ref = "**/" + ref;
            }
            // any pattern ending with a / or \ would match nothing, so is considered to match all paths beneath it
            // (Ant does this too http://ant.apache.org/manual/dirtasks.html)
            if (ref.endsWith("/") || ref.endsWith("\\"))
            {
                ref += "**";
            }

            restrictedRefs.add(ref);
        }
        return restrictedRefs;
    }

    private boolean isRestrictedRef(List<String> restrictedRefs, RefChange refChange)
    {
        String refId = refChange.getRefId();
        for(String refPattern : restrictedRefs)
        {
            if(pathMater.match(refPattern, refId))
            {
                return true;
            }
        }

        return false;
    }

    private boolean isForcePush(Repository repository, RefChange refChange)
    {
        // A force push is a non fast forward update.
        // This means that only changes are added to the branch.
        // Hence we can detect a force push by detecting if changes were removed
        return refChange.getType() == RefChangeType.UPDATE && commitService.getChangesetsBetween(
                new ChangesetsBetweenRequest.Builder(repository)
                        .exclude(refChange.getToHash())
                        .include(refChange.getFromHash())
                        .build(),
                PageUtils.newRequest(0, 1)
        ).getSize() > 0;
    }
}
